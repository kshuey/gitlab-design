### Problem

<!-- What’s the problem? Why is it worth solving? -->

### Solution

<!-- What’s the solution? Why is it like that? What are the benefits?) -->

### Example(s)

<!-- One or more images showing the solution. They don’t have to be in GitLab. -->

### Usage

<!-- When do you use this pattern? And how? -->

#### Dos and dont's

<!-- Use this table to add images and text describing what’s ok and not ok. -->

| :white_check_mark:  Do | :stop_sign: Don’t |
|------------------------|-------------------|
|  |  |

### Related patterns

<!-- List any related or similar solutions. If none, write: No related patterns -->

### Links / references

<!-- Add external links and references if necessary -->

### Checklist

Make sure these are completed before closing the issue, with a link to the
relevant commit or issue, if applicable. Get familiar with the [Sketch UI Kit
documentation](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md)
which has information on updating files, structure, fonts, and symbols.

1. [ ] **Author**: Create a Sketch file in your progress folder with the
   changes required for this issue. Try to use existing symbols, layer styles,
   and text styles.
1. [ ] **Author**: Ask another Product Designer to review your personal Sketch
   file, linking them to your latest commit so they know where to find it. If
   they have the capacity, they should assign themselves to this issue. If not,
   try pinging another person.
1. [ ] **Reviewer**: Review and approve author's changes in their personal
   Sketch file, according to the [workflow](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md#sketch-workflow).
1. [ ] **Author**: Add your changes to the GitLab Sketch UI Kit (pattern
   library and/or instance sheet), following this [step-by-step process](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md#when-changes-are-approved).
1. [ ] **Author**: Ask the reviewer to review your changes to the Sketch UI
   Kit files.
1. [ ] **Reviewer**: Review and approve author's changes to the Sketch UI Kit
   files, according to the [workflow](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/sketch-ui-kit.md#sketch-workflow).
1. [ ] **Author**: [Create an issue in the Design
   System](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/new)
   to update the design **specs** and **documentation**. Mark it as related to
   this issue.
1. [ ] **Author**: Add a read only (FYI) agenda item to the next [UX weekly
   call](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit?usp=sharing)
   to inform everyone of these changes, linking to this issue.

/label ~"UX" ~"pattern library" ~Pajamas ~"pajamas::create"
/cc @gitlab-com/gitlab-ux
